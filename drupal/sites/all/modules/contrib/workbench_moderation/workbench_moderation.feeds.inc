<?php
/**
 * @file
 * Feeds mappers for Workbench Moderation.
 */

/**
 * Implements hook_feeds_processor_targets_alter().
 */
function workbench_moderation_feeds_processor_targets_alter(&$targets, $entity_type, $bundle_name){
  if ($entity_type == 'node' && workbench_moderation_node_type_moderated($bundle_name)) {
    $targets['workbench_moderation_state'] = array(
      'name' => t('Current workbench moderation state'),
      'callback' => 'workbench_moderation_feeds_set_target',
      'description' => t('The current moderation state of the node. Either a state machine name or label will work.'),
    );
  }
}

/**
 * Feeds target callback for setting the current workbench state.
 */
function workbench_moderation_feeds_set_target($source, $entity, $target, $values, $mapping) {
  $states = workbench_moderation_states();
  $current_state = FALSE;

  // Convert a single element array to a string.
  if (is_array($values) && count($values) === 1) {
    $values = array_pop($values);
  }

  if ($states[$values]) {
    $current_state = $values;
  }
  else {
    foreach ($states as $state) {
      if ($state->label == trim($values)) {
        $current_state = $state->name;
        break;
      }
    }
  }

  // Only set a new current state if we have a valid new state.
  if ($current_state !== FALSE) {
    $entity->workbench_moderation_state_new = $current_state;
  }
}
