/**
 * PARA COMPRIMIR IMAGENES EN FORMATO JPG, PNG, SVG
 * DEBE DE COLOCAR LA IMAGEN ORIGINAL EN LA CARPETA /gulp_imagenmin/src Y LA COMPRIMIDA SE GENERARA EN /gulp_imagenmin/dest
 * https://www.npmjs.com/package/gulp-imagemin/
 */
const gulp = require('gulp');
const imagemin = require('gulp-imagemin');

gulp.task('imagenmin', () =>
  gulp.src('gulp-imagenmin/src/*')
    .pipe(imagemin())
    .pipe(gulp.dest('gulp-imagenmin/dest'))
);