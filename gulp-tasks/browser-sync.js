/**
 * Browsersync + Gulp.js - https://www.browsersync.io/docs/gulp
 * ES IMPORTANTE CAMBIAR LA RUTA DEL TEMA CUSTOM EN LA LINEA 13
 * ES IMPORTANTE CAMBIAR LA RUTA URL LOCAL EN LA LINE 27
 */

var gulp = require('gulp');
var browserSync = require('browser-sync');

//
// Path settings
//
var themeDir = 'drupal/web/themes/bootstrap_geducar';

// This is needed because we're not using compass-options anymore.
var paths = {
    js: themeDir + '/js',
    css: themeDir + '/css'
};

/**
 * Task for running browserSync.
 */
gulp.task('browserSync', function () {
    'use strict';
    browserSync.init(null, {
        proxy: 'local.drupal8.developer',
        files: [
            paths.css,
            paths.js
        ]
    });
});