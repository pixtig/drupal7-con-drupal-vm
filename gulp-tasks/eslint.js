/**
 * @file
 * Validate custom JavaScript with best practices and Drupal Coding Standards.
 * AQUI ES NECESARIO CAMBIAR LA RUTA DEL TEMA CUSTOM EN LA LINEA 20
 */
/* eslint-env node */

var gulp = require('gulp');
var eslint = require('gulp-eslint');
var fs = require('fs');
var argv = require('minimist')(process.argv.slice(2));
var gulpif = require('gulp-if');

gulp.task('eslint', function () {
  'use strict';
  var sourcePatterns = [
    'gulpfile.js',
    'gulp-tasks/*.js',
    'public_html/drupal/web/modules/**/*.js',
    'public_html/drupal/web/themes/bootstrap_geducar/**/*.js'
  ];
  var writeOutput = argv.hasOwnProperty('outputfile');
  var wstream;
  if (writeOutput) {
    wstream = fs.createWriteStream(argv.outputfile + '/eslint.xml');
  }
  var result = gulp.src(sourcePatterns)
    .pipe(eslint({
      configFile: './.eslintrc'
    }))
    .pipe(eslint.format())
    .pipe(gulpif(writeOutput, eslint.format('junit', wstream)))
    .pipe(eslint.failAfterError());

  return result;
});